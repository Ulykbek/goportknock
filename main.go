package main

import (
	"flag"
	"fmt"
	"net"
	"sync"
)

func main() {
	fmt.Println("Usage: go run main.go -hostname=example.com -ports=top/all -protocol=tcp/udp")
	hostPtr := flag.String("hostname","localhost","a string")
	portsPtr := flag.String("ports", "top", "a string")
	protocolPtr := flag.String("protocol","tcp", "a string")
	flag.Parse()

	var ports int

	if *portsPtr == "top" {
		ports = 1024
	}else if *portsPtr == "all" {
		ports = 65535
	}


	var wg sync.WaitGroup
	for i:=1; i <= ports; i++ {
		wg.Add(1)
		go func(j int) {
			defer wg.Done()
			address := fmt.Sprintf("%s:%d",*hostPtr,j)
			conn, err := net.Dial(*protocolPtr,address)
			if err != nil {
				return
			}
			conn.Close()
			fmt.Printf("%d open\n",j)
		}(i)
	}
	wg.Wait()
}

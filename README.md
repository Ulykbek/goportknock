# goportknock

Port scanner in Go

Usage: `go run main.go -hostname=example.com`

Default protocol: tcp, you can change to udp using flag `-protocol=udp`
Default ports: top 1024, you can change to scan all ports 65535 using flag `-ports=all`

Example: `go run main.go -hostname=example.com -ports=top/all -protocol=tcp/udp`